//
//  ViewController.swift
//  Top20Giphy
//
//  Created by Thibaut Brunel on 19/07/2017.
//  Copyright © 2017 Thibaut Brunel. All rights reserved.
//

import UIKit
import SwiftyGif

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var gifsUrls: [String]!
    var gifsData: [Data]!

    @IBOutlet weak var gifsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gifsData = [Data]()
        
        ServerRequests.get20TrendingGifs(completionHandler: { json in
            if json != nil {
                self.gifsUrls = Utils.parseTop20GifsJSON(json: json!)
                
                self.downloadGifs()
            } else {
                let alert = UIAlertController(title: "Error", message: "An error occurred, please try again later.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func downloadGifs() {
//        for gifUrl in self.gifsUrls {
//            let gifData = Utils.getGifData(atUrl: gifUrl)
        let gifData = Utils.getGifData(atUrl: self.gifsUrls[0])
        let gifData1 = Utils.getGifData(atUrl: self.gifsUrls[1])
        let gifData2 = Utils.getGifData(atUrl: self.gifsUrls[2])
        
            guard gifData != nil else {
//                continue
                return
            }
        
            self.gifsData.append(gifData!)
        self.gifsData.append(gifData1!)
        self.gifsData.append(gifData2!)
            
            self.gifsTableView.reloadData()
//        }
    }
    
    // MARK: UITableViewDelegate and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gifsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GifTableViewCell", for: indexPath) as! GifTableViewCell
        
        cell.gifImageView.setGifImage(UIImage(gifData: self.gifsData[indexPath.row]))
        
        return cell
    }

}
