//
//  Utils.swift
//  Top20Giphy
//
//  Created by Thibaut Brunel on 19/07/2017.
//  Copyright © 2017 Thibaut Brunel. All rights reserved.
//

import UIKit

class Utils {
    
    static func parseTop20GifsJSON(json: [String : Any]) -> [String] {
        var res = [String]()
        
        if let data = json["data"] as? [[String : Any]] {
            for gifInfo in data {
                if let images = gifInfo["images"] as? [String : Any] {
                    if let fixedHeight = images["fixed_height"] as? [String : Any] {
                        if let url = fixedHeight["url"] as? String {
                            res.append(url)
                        }
                    }
                }
            }
        }
        
        return res
    }
    
    class func getGifData(atUrl urlString: String) -> Data? {
        var imageData: Data?
        let url = URL(string: urlString)!
        
        do {
            imageData = try Data(contentsOf: url)
        } catch {
            imageData = nil
        }
        
        return imageData
    }
    
}
