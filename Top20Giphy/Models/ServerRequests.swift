//
//  ServerRequests.swift
//  Top20Giphy
//
//  Created by Thibaut Brunel on 19/07/2017.
//  Copyright © 2017 Thibaut Brunel. All rights reserved.
//

import Foundation
import Alamofire

class ServerRequests {
    
    static func get20TrendingGifs(completionHandler: @escaping ([String : Any]?) -> Void) {
        Alamofire.request("https://api.giphy.com/v1/gifs/trending?api_key=9f525431ce3a45d99c53458d77c2d6e8&limit=20").responseJSON { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? [String : Any] {
                    completionHandler(json)
                } else {
                    completionHandler(nil)
                }
            } else {
                completionHandler(nil)
            }
        }
    }
    
}
