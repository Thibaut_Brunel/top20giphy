//
//  GifTableViewCell.swift
//  Top20Giphy
//
//  Created by Thibaut Brunel on 19/07/2017.
//  Copyright © 2017 Thibaut Brunel. All rights reserved.
//

import UIKit

class GifTableViewCell: UITableViewCell {

    @IBOutlet weak var gifImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
