## Top20Giphy
Cette application, à son lancement, récupère les 20 gifs les plus populaires de Giphy (parsing du JSON retourné pour récupérer les URL).
Elle parcourt ensuite la liste des URL et récupère la `Data` des fichiers pour pouvoir les afficher.

### Librairies utilisées
- Alamofire pour la récupération des 20 gifs en tendance
- SwiftyGif pour l'affichage des gifs dans une `UIImageView`

### Rendu final
Le rendu final est une version qui fonctionne : elle affiche les 3 premiers gifs récupérés "en dur" dans la liste des URL.
Cependant, lorsque j'essaye d'afficher les 20 gifs, l'application crashe (trop de gifs créés d'un coup). Il aurait fallu (peut-être avec RxSwift) mettre un délai entre deux créations de gifs pour pouvoir éviter ce crash.
Je n'ai malheureusement pas eu le temps de commenter mon code. Je me suis rendu compte (trop tard) que j'aurais peut-être dû arrêter d'essayer de résoudre ce crash, laisser directement la version qui fonctionne avec uniquement 3 gifs affichés, et plutôt commenter ce que j'avais déjà réalisé...
